# Arisa #

Arisa is a simple starter Discord bot written in Node.Js.

### Basic commands ###

* Random image or gif
* Imgur search
* Youtube search
* Youtube random video
* Get anime info by title
* Get nosue signature
* Get some osu stats

### TODO ###

* More basic commands
* Improve configuration

### Maybe in future ###

* Website
* Blacklist

### Install guide ###

1. Clone or download repo
2. Run `npm install`
3. Edit config
4. Run `node app.js`

### Config ###

Don't copy it!

```
{
        "options": {
          "www": true, // Website on/off. Default: true
          "commandsFile": "./commands.js" // Here you can specify location of your commands
        },
        "discord": {
            "token": "enter your token" // Discord token
        },
        "mal": { // MyAnimeList settings. It needs for anime search
          "MAL_USER": "username", 
          "MAL_PASS": "password"
        },
        "osu": { // Osu api key for user stats
          "api_key": "enter your osu api key"
        },
        "db": { // no comments
          "host": "please",
          "user": "just",
          "password": "ignore",
          "database": "it"
        },
        "youtube": { // Youtube api key for search
          "key": "enter your youtube api key"
        },
        "imgur": { // Imgur api key for search
          "key": "enter your imgur api key"
        }
}
```