var request = require("request");
var xml2js = require("xml2js");
var unirest = require("unirest");
var ent = require("entities");
var osuapi = require("osu-api");
var Imgur = require("imgur-search");
var Youtube = require('youtube-node');

var config = require("./config.json");

var MAL_USER = config.mal.MAL_USER;
var MAL_PASS = config.mal.MAL_PASS;
var osu = new osuapi.Api(config.osu.token);
var youtube_key = config.youtube.key;
var imgur_key = config.imgur.key;

var commands = {
  "name": {
    process: function (bot, msg, suffix) {
      var tags = suffix.split(" ");
      bot.updateDetails({username: tags}, function(err) {
        if (err) {
          console.log(err);
        }
        else {
          bot.sendMessage(msg.channel, 'Ура, теперь меня зовут ' + tags + ' (• ε •)');
        }
      });
    }
  },
  "game": {
    process: function (bot, msg, suffix) {
      var tags = suffix.split(" ");
      console.log(tags[0]);
      bot.setPlayingGame(tags[0], function (err) {
        if (err) console.log(err);
      });
      if (!tags) {
        bot.reply(msg, 'спасибо, теперь я играю в ' + tags + ' (• ε •)');
      }
      else bot.reply(msg, 'ну блиин...');
      }
    },
  "img": {
    process: function (bot, msg, suffix) {
        tags = suffix.split(" ");
        search = new Imgur(imgur_key);
        search.search(tags).then(function(results) {
          if (results === undefined || results.length === 0) {
            bot.reply(msg, "no memes for you");
          }
          else {
            var image = results[Math.floor(Math.random() * results.length)];
            bot.sendFile(msg, image.link);
          }
        });
    }
  },
  "nosue": {
    process: function (bot, msg, suffix) {
      tags = suffix.split(" ");
      url = "http://nosue.syphist.com/sig/?u=" + tags + "&m=0";
      bot.reply(msg, "here comes dat sig" + "\n" + url);
    }
  },
  "youtube": {
    process: function (bot, msg, suffix) {
          tags = suffix.split(" ");
          if (tags) {
          youtube = new Youtube();
          youtube.setKey(youtube_key);
          youtube.addParam('type', 'video');
          youtube.search(tags, 1, function(error, result) {
          if (error) {
            bot.reply(msg, 'no more memes for you, faggot');
          }
          if (!result || !result.items || result.items.length < 1) {
            bot.reply(msg, "the internet run out of space");
          }
          else {
            bot.reply(msg, "http://www.youtube.com/watch?v=" + result.items[0].id.videoId);
          }
       });
      }
    }
  },
  "ping": {
    process: function(bot,msg,suffix) {
      if(suffix){
        var users = msg.channel.server.members.getAll("username",suffix);
        if(users.length == 1){
          bot.sendMessage(msg.channel, "The id of " + users[0] + " is " + users[0].id);
        } else if(users.length > 1){
          var response = "multiple users found:";
          for(var i=0;i<users.length;i++){
            var user = users[i];
            response += "\nThe id of " + user + " is " + user.id;
          }
          bot.sendMessage(msg.channel,response);
        } else {
          bot.sendMessage(msg.channel,"No user " + suffix + " found!");
        }
      } else {
        bot.sendMessage(msg.channel, "The id of " + msg.author + " is " + msg.author.id);
      }
    }
  },
  "iq": {
    process: function (bot, msg, suffix) {
          tags = suffix.split(" ");
          iq = Math.floor((Math.random() * 200) + 1);
          bot.reply(msg, tags + "'s iq is " + iq);
    }
  },
  "anime": {
    process: function(bot, msg, suffix) {
      tags = suffix.split(" ");
      var rUrl = "http://myanimelist.net/api/anime/search.xml?q=" + tags;
      request(rUrl, {"auth": {"user": MAL_USER, "pass": MAL_PASS, "sendImmediately": false}}, function(error, response, body) {
        if (error) console.log(error);
        else if (!error && response.statusCode == 200) {
          xml2js.parseString(body, function(err, result) {
            var title = result.anime.entry[0].title;
              _english = result.anime.entry[0].english;
              _ep = result.anime.entry[0].episodes;
              _score = result.anime.entry[0].score;
              _type = result.anime.entry[0].type;
              _image = result.anime.entry[0].image;
              _status = result.anime.entry[0].status;
              synopsis = result.anime.entry[0].synopsis.toString();
              _id = result.anime.entry[0].id;
            synopsis = ent.decodeHTML(synopsis.replace(/<br \/>/g, " ").replace(/\[(.{1,10})\]/g, "").replace(/\r?\n|\r/g, " ").replace(/\[(i|\/i)\]/g, "*").replace(/\[(b|\/b)\]/g, "**"));
            bot.sendMessage(msg.channel, "**" + title + " / " + _english + "**\n**Type:** " + _type + " **| Episodes:** " + _ep + " **| Status:** " + _status + " **| Score:** " + _score + "\n" + synopsis + "\n**<http://www.myanimelist.net/anime/" + _id + ">**" + "\n" + _image);
      });
    }
  });
    }
  }
};

module.exports.commands = commands;