var express = require('express');
var app = new express();
var cool = require('cool-ascii-faces');

module.exports.run = function() {
	app.set('port', (process.env.PORT || 5000));

	app.get('/', function(request, response) {
		response.send(cool());
	});

	app.listen(app.get('port'), function() {
		console.log('Server is running on port', app.get('port'));
	});
};