try {
  var Discord = require("discord.js");
  var fs = require("fs");
  var mysql = require('mysql');
  var config = require("./config.json");
  var commands = require(config.options.commandsFile);
}
catch (e) {
  console.log('Module error, try running npm install command.');
}

/* Just ignore it
var db = mysql.createConnection({
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database
});
*/

if (config.options.www) {
  var web = require("./www.js");
  web.run();
}

var bot = new Discord.Client();

bot.on('ready', function() {
  console.log("Ready to begin! Serving in " + bot.channels.length + " channels");
});

bot.on("message", function(msg) {
  if (msg.author.id != bot.user.id && (msg.content[0] === '!' || msg.content.indexOf(bot.user.mention()) === 0)) {
    console.log("treating " + msg.content + " from " + msg.author + " as command");
    var cmdTxt = msg.content.split(" ")[0].substring(1);
    var suffix = msg.content.substring(cmdTxt.length + 2);
    if (msg.content.indexOf(bot.user.mention()) === 0) {
      try {
        cmdTxt = msg.content.split(" ")[1];
        suffix = msg.content.substring(bot.user.mention().length + cmdTxt.length + 2);
      } catch (e) {
        bot.sendMessage(msg.channel, "Yes?");
        return;
      }
    }
    var cmd = commands.commands[cmdTxt];
      if (cmd) {
      try {
          cmd.process(bot, msg, suffix);
      } catch (e) {
        if (e) {
          bot.sendMessage(msg.channel, "command " + cmdTxt + " failed :(\n" + e.stack);
        }
      }
    } else {
      if (true) {
        bot.sendMessage(msg.channel, "Invalid command " + cmdTxt);
      }
    }
    } else {
    if (msg.author == bot.user) {
      return;
    }

    if (msg.author != bot.user && msg.isMentioned(bot.user)) {
      bot.sendMessage(msg.channel, msg.author + ", you called?");
    }
  }
});

bot.loginWithToken(config.discord.token);
